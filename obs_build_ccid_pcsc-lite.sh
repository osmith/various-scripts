#!/bin/sh -ex
# Build OBS source packages for ccid and pcsc-lite

# Versions of ccid an pcsc-lite. When rebasing on the most recent debian
# packaging, change these to the current versions in debian/changelog.
CCID_VER="1.5.0"
PCSC_LITE_VER="1.9.9"

# Parent dir of git clones of ccid, pcsc-lite from
# https://gitea.osmocom.org/osmith/ccid
# https://gitea.osmocom.org/osmith/pcsc-lite
SRCDIR=~/code/

tempdir=/tmp/deb
distfiles=/tmp/distfiles

# Download upstream tarballs. The debian git repositories contain some of the
# sources, but as specified in debian/source/format "3.0 (quilt)",
# dpkg-buildpackage actually uses upstream tarballs instead of the bundled
# sources. To patch the sources, debian/patches has to be used.
mkdir -p "$distfiles"
CCID_TARBALL="ccid_$CCID_VER.orig.tar.bz2"
if ! [ -e "$distfiles/$CCID_TARBALL" ]; then
	wget "https://ccid.apdu.fr/files/ccid-$CCID_VER.tar.bz2" \
		-O "$distfiles/$CCID_TARBALL"
fi
PCSC_LITE_TARBALL="pcsc-lite_$PCSC_LITE_VER.orig.tar.bz2"
if ! [ -e "$distfiles/$PCSC_LITE_TARBALL" ]; then
	wget "https://pcsclite.apdu.fr/files/pcsc-lite-$PCSC_LITE_VER.tar.bz2" \
		-O "$distfiles/$PCSC_LITE_TARBALL"
fi

# Prepare tempdir with upstream sources
rm -rf "$tempdir"
mkdir "$tempdir"
cp "$distfiles/$CCID_TARBALL" \
	"$distfiles/$PCSC_LITE_TARBALL" \
	"$tempdir"

# Install build deps, build binary and source packages
for i in ccid pcsc-lite; do
	cp -r "$SRCDIR"/"$i" /tmp/deb/"$i"
	cd /tmp/deb/"$i"
	sudo apt build-dep .
	dpkg-buildpackage -us -uc -j17
	rm -rf /tmp/deb/"$i"

	cd /tmp/deb
	rm *.buildinfo *.changes *.deb *.ddeb
	mkdir "$i"
	mv "$i"*.* "$i"
done

tree /tmp/deb
