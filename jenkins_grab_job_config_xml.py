#!/usr/bin/env python3
# Dump jenkins config.xml via web interface, in case other methods such as the
# cli api don't work.
import re
import requests

session = requests.Session()
api_url = CHANGEME
username = CHANGEME
password = CHANGEME

# Login
print(f"GET {api_url}/login/")
session.get(f"{api_url}/login/")
payload = {
    "j_username": username,
    "j_password": password,
    "from": "",
    "submit": "Sign+in",
    "remember_me": "on",
}
print(f"POST {api_url}/j_sprint_security_check")
r = session.post(f"{api_url}/j_spring_security_check", data=payload)
main_page = r.text
assert "log out" in main_page

# Find jobs
re_job_id = re.compile('<tr id="job_(.*?)"')
jobs = []
for match in re_job_id.findall(main_page):
    jobs += [match]
with open("out/_job_list.txt", "w") as handle:
    handle.write("\n".join(jobs))

# Get config.xml for all jobs
for i, job in enumerate(jobs):
    url = f"{api_url}/job/{job}/config.xml"
    print(f"({i+1}/{len(jobs)}) GET {url}")
    r = session.get(url)

    with open(f"out/{job}.xml", "w") as handle:
        handle.write(r.text)

    assert r.status_code == 200

print("done")
